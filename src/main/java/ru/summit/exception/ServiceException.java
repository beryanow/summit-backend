package ru.summit.exception;

public class ServiceException extends RuntimeException {
    public ServiceException(String s) {
        super(s);
    }
}
