package ru.summit.entity.db;

import lombok.Data;
import ru.summit.entity.dto.SettingsDto;

import javax.persistence.*;

@Data
@Entity
@Table(name = "SETTINGS")
public class Settings {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private Long profileId;
    @Column(name = "GOALS_IMAGE_ID")
    private Long goalsScreenBackgroundId;
    @Column(name = "IDEAS_IMAGE_ID")
    private Long ideasScreenBackgroundId;
    @Column(name = "NUTRITION_IMAGE_ID")
    private Long nutritionScreenBackgroundId;
    @Column(name = "TRAINING_IMAGE_ID")
    private Long trainingScreenBackgroundId;
    @Column(name = "PROFILE_IMAGE_ID")
    private Long profileScreenBackgroundId;
    @Column(name = "WELCOME_IMAGE_ID")
    private Long welcomeScreenBackgroundId;

    public SettingsDto dto() {
        final SettingsDto dto = new SettingsDto();
        dto.setId(getId());
        dto.setProfileId(getProfileId());
        dto.setGoalsScreenBackgroundId(getGoalsScreenBackgroundId());
        dto.setIdeasScreenBackgroundId(getIdeasScreenBackgroundId());
        dto.setNutritionScreenBackgroundId(getNutritionScreenBackgroundId());
        dto.setTrainingScreenBackgroundId(getTrainingScreenBackgroundId());
        dto.setProfileScreenBackgroundId(getProfileScreenBackgroundId());
        dto.setWelcomeScreenBackgroundId(getWelcomeScreenBackgroundId());
        return dto;
    }
}
