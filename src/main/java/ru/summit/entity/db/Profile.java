package ru.summit.entity.db;

import lombok.Data;
import ru.summit.entity.dto.ProfileDto;

import javax.persistence.*;

@Data
@Entity
@Table(name = "PROFILE")
public class Profile {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private String surname;
    private Integer age;
    private Long imageId;
    private String email;
    private Long userId;

    public ProfileDto dto() {
        final ProfileDto dto = new ProfileDto();
        dto.setId(getId());
        dto.setName(getName());
        dto.setSurname(getSurname());
        dto.setAge(getAge());
        dto.setImageId(getImageId());
        dto.setEmail(getEmail());
        dto.setUserId(getUserId());
        return dto;
    }
}
