package ru.summit.entity.db;

import lombok.Data;
import ru.summit.entity.dto.TaskDto;

import javax.persistence.*;

@Data
@Entity
@Table(name = "TASK")
public class Task {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private Long goalId;
    private String name;
    private String description;
    private String stage;
    private Long userId;

    public TaskDto dto() {
        final TaskDto dto = new TaskDto();
        dto.setId(getId());
        dto.setName(getName());
        dto.setDescription(getDescription());
        dto.setStage(getStage());
        dto.setUserId(getUserId());
        return dto;
    }
}
