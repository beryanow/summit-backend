package ru.summit.entity.db;

import lombok.Data;
import ru.summit.entity.dto.ImageDto;

import javax.persistence.*;

@Data
@Entity
@Table(name = "IMAGE")
public class Image {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private byte[] content;

    public ImageDto dto() {
        final ImageDto dto = new ImageDto();
        dto.setId(id);
        dto.setContent(content);
        return dto;
    }
}
