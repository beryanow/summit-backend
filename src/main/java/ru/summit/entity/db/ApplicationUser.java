package ru.summit.entity.db;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.summit.entity.dto.UserDto;

import javax.persistence.*;

@Data
@Entity
@Table(name = "USERS")
@AllArgsConstructor
@NoArgsConstructor
public class ApplicationUser {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String username;
    private String password;

    public ApplicationUser(UserDto userDto) {
        username = userDto.getUsername();
        password = userDto.getPassword();
    }
}
