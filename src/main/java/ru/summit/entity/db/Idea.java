package ru.summit.entity.db;

import lombok.Data;
import ru.summit.entity.dto.IdeaDto;

import javax.persistence.*;

@Data
@Entity
@Table(name = "IDEA")
public class Idea {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private String content;
    private Long backgroundId;
    private String size;
    private Long userId;

    public IdeaDto dto() {
        final IdeaDto dto = new IdeaDto();
        dto.setId(id);
        dto.setName(name);
        dto.setContent(content);
        dto.setBackgroundId(backgroundId);
        dto.setSize(size);
        dto.setUserId(getUserId());
        return dto;
    }
}
