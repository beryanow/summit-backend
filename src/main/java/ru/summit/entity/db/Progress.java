package ru.summit.entity.db;

import lombok.Data;
import ru.summit.entity.dto.ProgressDto;

import javax.persistence.*;

@Data
@Entity
@Table(name = "PROGRESS")
public class Progress {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private Long profileId;
    private Long imageId;
    private String date;

    public ProgressDto dto() {
        final ProgressDto dto = new ProgressDto();
        dto.setId(getId());
        dto.setProfileId(getProfileId());
        dto.setImageId(getImageId());
        dto.setDate(getDate());
        return dto;
    }
}
