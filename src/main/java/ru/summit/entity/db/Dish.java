package ru.summit.entity.db;

import lombok.Data;
import ru.summit.entity.dto.DishDto;

import javax.persistence.*;

@Data
@Entity
@Table(name = "DISH")
public class Dish {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private String description;
    private String recipe;
    @Column(name = "BACKGROUND_ID")
    private Long backgroundId;
    private String size;
    private Long userId;

    public DishDto dto() {
        final DishDto dto = new DishDto();
        dto.setId(getId());
        dto.setName(getName());
        dto.setDescription(getDescription());
        dto.setRecipe(getRecipe());
        dto.setBackgroundId(getBackgroundId());
        dto.setSize(getSize());
        dto.setUserId(getUserId());
        return dto;
    }
}
