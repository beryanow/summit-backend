package ru.summit.entity.db;

import lombok.Data;
import ru.summit.entity.dto.GoalDto;

import javax.persistence.*;

@Data
@Entity
@Table(name = "GOAL")
public class Goal {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private String description;
    private Long backgroundId;
    private String size;
    private String statePlace;
    private Long userId;

    public GoalDto dto() {
        final GoalDto dto = new GoalDto();
        dto.setId(getId());
        dto.setName(getName());
        dto.setDescription(getDescription());
        dto.setBackgroundId(getBackgroundId());
        dto.setSize(getSize());
        dto.setStatePlace(getStatePlace());
        dto.setUserId(getUserId());
        return dto;
    }
}
