package ru.summit.entity.db;

import lombok.Data;
import ru.summit.entity.dto.IngredientDto;

import javax.persistence.*;

@Data
@Entity
@Table(name = "INGREDIENT")
public class Ingredient {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private Long dishId;
    private String name;
    private String amount;
    private Long userId;

    public IngredientDto dto() {
        final IngredientDto dto = new IngredientDto();
        dto.setId(getId());
        dto.setName(getName());
        dto.setAmount(getAmount());
        dto.setUserId(getUserId());
        return dto;
    }
}
