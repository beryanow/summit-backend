package ru.summit.entity.db;

import lombok.Data;
import ru.summit.entity.dto.TrainingScheduleDto;

import javax.persistence.*;

@Data
@Entity
@Table(name = "TRAINING_SCHEDULE")
public class TrainingSchedule {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private Long trainingId;
    private String date;

    public TrainingScheduleDto dto() {
        final TrainingScheduleDto dto = new TrainingScheduleDto();
        dto.setId(getId());
        dto.setTrainingId(getTrainingId());
        dto.setDate(getDate());
        return dto;
    }
}
