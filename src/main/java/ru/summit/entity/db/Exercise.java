package ru.summit.entity.db;

import lombok.Data;
import ru.summit.entity.dto.ExerciseDto;

import javax.persistence.*;

@Data
@Entity
@Table(name = "EXERCISE")
public class Exercise {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private String description;
    private String type;
    private Integer repetitions;
    private Long trainingId;

    public ExerciseDto dto() {
        final ExerciseDto dto = new ExerciseDto();
        dto.setId(getId());
        dto.setName(getName());
        dto.setDescription(getDescription());
        dto.setType(getType());
        dto.setRepetitions(getRepetitions());
        dto.setTrainingId(getTrainingId());
        return dto;
    }
}
