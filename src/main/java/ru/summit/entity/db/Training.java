package ru.summit.entity.db;

import lombok.Data;
import ru.summit.entity.dto.TrainingDto;

import javax.persistence.*;

@Data
@Entity
@Table(name = "TRAINING")
public class Training {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private Long userId;
    private String name;
    private String cycle;

    public TrainingDto dto() {
        final TrainingDto dto = new TrainingDto();
        dto.setId(getId());
        dto.setUserId(getUserId());
        dto.setName(getName());
        dto.setCycle(getCycle());
        return dto;
    }
}
