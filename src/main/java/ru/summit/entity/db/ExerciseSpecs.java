package ru.summit.entity.db;

import lombok.Data;
import ru.summit.entity.dto.ExerciseSpecsDto;

import javax.persistence.*;

@Data
@Entity
@Table(name = "EXERCISE_SPECS")
public class ExerciseSpecs {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private String description;
    private String type;
    private Integer repetitions;

    public ExerciseSpecsDto dto() {
        final ExerciseSpecsDto dto = new ExerciseSpecsDto();
        dto.setId(getId());
        dto.setName(getName());
        dto.setDescription(getDescription());
        dto.setType(getType());
        dto.setRepetitions(getRepetitions());
        return dto;
    }
}
