package ru.summit.entity.core;

public enum Cycle {
    LIGHT,
    MEDIUM,
    HARD,
    PERSONAL;

    @Override
    public String toString() {
        switch (this) {
            case LIGHT:
                return "Легкий";
            case MEDIUM:
                return "Средний";
            case HARD:
                return "Сложный";
            case PERSONAL:
                return "Персональный";
            default:
                return "";
        }
    }

    public static Cycle byName(String name) {
        final String s = name.toLowerCase();
        switch (s) {
            case "легкий":
                return LIGHT;
            case "средний":
                return MEDIUM;
            case "сложный":
                return HARD;
            case "персональный":
                return PERSONAL;
        }
        return null;
    }
}
