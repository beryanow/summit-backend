package ru.summit.entity.core;

public enum Type {
    GYM,
    STREET;

    public static Type byName(String name) {
        final String s = name.toLowerCase();
        switch (s) {
            case "зал":
                return GYM;
            case "улица":
                return STREET;
        }
        return null;
    }

    @Override
    public String toString() {
        switch (this) {
            case GYM:
                return "Зал";
            case STREET:
                return "Улица";
            default:
                return "";
        }
    }
}
