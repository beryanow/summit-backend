package ru.summit.entity.core;

public enum Stage {
    NEW,
    IN_PROGRESS,
    DONE;

    @Override
    public String toString() {
        switch (this) {
            case NEW:
                return "New";
            case IN_PROGRESS:
                return "In progress";
            case DONE:
                return "Done";
            default:
                return "";
        }
    }

    public static Stage byName(String name) {
        final String s = name.toLowerCase();
        switch (s) {
            case "new":
                return NEW;
            case "in progress":
                return IN_PROGRESS;
            case "done":
                return DONE;
        }
        return null;
    }

}
