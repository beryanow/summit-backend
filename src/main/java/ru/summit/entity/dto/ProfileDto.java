package ru.summit.entity.dto;

import lombok.Data;
import ru.summit.entity.db.Profile;

@Data
public class ProfileDto {
    private Long id;
    private String name;
    private String surname;
    private Integer age;
    private Long imageId;
    private String email;
    private SettingsDto settingsDto;
    private Long userId;

    public Profile profile() {
        final Profile profile = new Profile();
        profile.setId(getId());
        profile.setName(getName());
        profile.setSurname(getSurname());
        profile.setAge(getAge());
        profile.setImageId(getImageId());
        profile.setEmail(getEmail());
        profile.setUserId(getUserId());
        return profile;
    }
}
