package ru.summit.entity.dto;

import lombok.Data;
import ru.summit.entity.db.Image;

@Data
public class ImageDto {
    private Long id;
    private byte[] content;

    public Image image() {
        final Image image = new Image();
        image.setId(id);
        image.setContent(content);
        return image;
    }
}
