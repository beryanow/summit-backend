package ru.summit.entity.dto;

import lombok.Data;
import ru.summit.entity.db.Progress;

@Data
public class ProgressDto {
    private Long id;
    private Long profileId;
    private Long imageId;
    private String date;

    public Progress progress() {
        final Progress progress = new Progress();
        progress.setId(getId());
        progress.setProfileId(getProfileId());
        progress.setImageId(getImageId());
        progress.setDate(getDate());
        return progress;
    }
}
