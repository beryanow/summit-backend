package ru.summit.entity.dto;

import lombok.Data;
import ru.summit.entity.db.ExerciseSpecs;

@Data
public class ExerciseSpecsDto {
    private Long id;
    private String name;
    private String description;
    private String type;
    private Integer repetitions;

    public ExerciseSpecs exerciseSpecs() {
        final ExerciseSpecs exerciseSpecs = new ExerciseSpecs();
        exerciseSpecs.setId(getId());
        exerciseSpecs.setName(getName());
        exerciseSpecs.setDescription(getDescription());
        exerciseSpecs.setType(getType());
        exerciseSpecs.setRepetitions(getRepetitions());
        return exerciseSpecs;
    }
}
