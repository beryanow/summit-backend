package ru.summit.entity.dto;

import lombok.Data;
import ru.summit.entity.db.Training;

import java.util.Collection;

@Data
public class TrainingDto {
    private Long id;
    private Long userId;
    private String name;
    private String cycle;
    private Collection<ExerciseDto> exercises;
    private Collection<TrainingScheduleDto> trainingSchedules;

    public Training training() {
        final Training training = new Training();
        training.setId(getId());
        training.setUserId(getUserId());
        training.setName(getName());
        training.setCycle(getCycle());
        return training;
    }
}
