package ru.summit.entity.dto;

import lombok.Data;
import ru.summit.entity.db.Idea;

@Data
public class IdeaDto {
    private Long id;
    private String name;
    private String content;
    private Long backgroundId;
    private String size;
    private Long userId;

    public Idea idea() {
        final Idea idea = new Idea();
        idea.setId(id);
        idea.setName(name);
        idea.setContent(content);
        idea.setBackgroundId(backgroundId);
        idea.setSize(size);
        idea.setUserId(getUserId());
        return idea;
    }
}
