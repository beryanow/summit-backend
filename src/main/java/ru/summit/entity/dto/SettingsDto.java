package ru.summit.entity.dto;

import lombok.Data;
import ru.summit.entity.db.Settings;

@Data
public class SettingsDto {
    private Long id;
    private Long profileId;
    private Long goalsScreenBackgroundId;
    private Long ideasScreenBackgroundId;
    private Long nutritionScreenBackgroundId;
    private Long trainingScreenBackgroundId;
    private Long profileScreenBackgroundId;
    private Long welcomeScreenBackgroundId;

    public Settings settings() {
        final Settings settings = new Settings();
        settings.setId(getId());
        settings.setProfileId(getProfileId());
        settings.setGoalsScreenBackgroundId(getGoalsScreenBackgroundId());
        settings.setIdeasScreenBackgroundId(getIdeasScreenBackgroundId());
        settings.setNutritionScreenBackgroundId(getNutritionScreenBackgroundId());
        settings.setTrainingScreenBackgroundId(getTrainingScreenBackgroundId());
        settings.setProfileScreenBackgroundId(getProfileScreenBackgroundId());
        settings.setWelcomeScreenBackgroundId(getWelcomeScreenBackgroundId());
        return settings;
    }
}
