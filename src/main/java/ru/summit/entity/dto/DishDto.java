package ru.summit.entity.dto;

import lombok.Data;
import ru.summit.entity.db.Dish;

import java.util.Collection;

@Data
public class DishDto {
    private Long id;
    private String name;
    private String description;
    private String recipe;
    private Long backgroundId;
    private String size;
    private Long userId;
    private Collection<IngredientDto> ingredients;

    public Dish dish() {
        final Dish dish = new Dish();
        dish.setId(getId());
        dish.setName(getName());
        dish.setDescription(getDescription());
        dish.setRecipe(getRecipe());
        dish.setBackgroundId(getBackgroundId());
        dish.setSize(getSize());
        dish.setUserId(getUserId());
        return dish;
    }
}
