package ru.summit.entity.dto;

import lombok.Data;
import ru.summit.entity.db.TrainingSchedule;

@Data
public class TrainingScheduleDto {
    private Long id;
    private Long trainingId;
    private String date;

    public TrainingSchedule trainingSchedule(Long trainingId) {
        final TrainingSchedule trainingSchedule = new TrainingSchedule();
        trainingSchedule.setId(getId());
        trainingSchedule.setTrainingId(trainingId);
        trainingSchedule.setDate(getDate());
        return trainingSchedule;
    }
}
