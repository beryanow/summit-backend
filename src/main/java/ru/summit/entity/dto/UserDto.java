package ru.summit.entity.dto;

import lombok.Data;

@Data
public class UserDto {
    private String username;
    private String password;
    private String name;
    private String surname;
    private String email;
    private int age;
}
