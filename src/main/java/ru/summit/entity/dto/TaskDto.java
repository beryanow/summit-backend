package ru.summit.entity.dto;

import lombok.Data;
import ru.summit.entity.db.Task;

@Data
public class TaskDto {
    private Long id;
    private String name;
    private String description;
    private String stage;
    private Long userId;

    public Task task(Long goalId) {
        final Task task = new Task();
        task.setId(getId());
        task.setGoalId(goalId);
        task.setName(getName());
        task.setDescription(getDescription());
        task.setStage(getStage());
        task.setUserId(getUserId());
        return task;
    }
}