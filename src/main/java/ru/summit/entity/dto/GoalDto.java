package ru.summit.entity.dto;


import lombok.Data;
import ru.summit.entity.db.Goal;

import java.util.Collection;

@Data
public class GoalDto {
    private Long id;
    private String name;
    private String description;
    private Long backgroundId;
    private String size;
    private String statePlace;
    private Long userId;
    private Collection<TaskDto> tasks;

    public Goal goal() {
        final Goal goal = new Goal();
        goal.setId(getId());
        goal.setName(getName());
        goal.setDescription(getDescription());
        goal.setBackgroundId(getBackgroundId());
        goal.setSize(getSize());
        goal.setStatePlace(getStatePlace());
        goal.setUserId(userId);
        return goal;
    }
}

