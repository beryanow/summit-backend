package ru.summit.entity.dto;

import lombok.Data;
import ru.summit.entity.db.Ingredient;

@Data
public class IngredientDto {
    private Long id;
    private String name;
    private String amount;
    private Long userId;

    public Ingredient ingredient(Long dishId) {
        final Ingredient ingredient = new Ingredient();
        ingredient.setId(getId());
        ingredient.setDishId(dishId);
        ingredient.setName(getName());
        ingredient.setAmount(getAmount());
        ingredient.setUserId(getUserId());
        return ingredient;
    }
}
