package ru.summit.entity.dto;

import lombok.Data;
import ru.summit.entity.db.Exercise;

@Data
public class ExerciseDto {
    private Long id;
    private String name;
    private String description;
    private String type;
    private Integer repetitions;
    private Long trainingId;

    public Exercise exercise(Long trainingId) {
        final Exercise exercise = new Exercise();
        exercise.setId(getId());
        exercise.setName(getName());
        exercise.setDescription(getDescription());
        exercise.setType(getType());
        exercise.setRepetitions(getRepetitions());
        exercise.setTrainingId(trainingId);
        return exercise;
    }
}
