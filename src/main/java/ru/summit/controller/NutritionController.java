package ru.summit.controller;

import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.summit.entity.dto.DishDto;
import ru.summit.service.NutritionService;

import java.util.Collection;

@RestController
public class NutritionController {

    private final NutritionService nutritionService;

    @Autowired
    public NutritionController(NutritionService nutritionService) {
        this.nutritionService = nutritionService;
    }

    @GetMapping("/api/nutrition/getAll")
    @ApiOperation(value = "Получить список блюд (рецептов)", notes = "Возвращает список объектов блюд (рецептов). " +
            "Блюдо содержит в себе список ингредиентов.")
    public ResponseEntity<Collection<DishDto>> getAll() {
        return ResponseEntity.ok(nutritionService.getAll());
    }

    @PutMapping("/api/nutrition")
    @ApiOperation(value = "Создать или изменить блюдо", notes = "Создает новое блюдо. " +
            "Если блюдо с таким id уже есть, перезаписывает.\n" +
            "Название блюда не может превышать 100 символов.\n" +
            "Описание блюда не может превышать 500 символов.\n" +
            "Рецепт блюда не может превышать 10000 символов.\n" +
            "Название ингредиента не может превышать 100 символов.\n" +
            "Количество ингредиента не может превышать 100 символов.\n")
    public ResponseEntity<DishDto> create(@RequestBody DishDto dishDto) {
        return ResponseEntity.ok(nutritionService.createOrChange(dishDto));
    }

    @DeleteMapping("/api/nutrition/delete/{id}")
    @ApiOperation(value = "Удалить блюдо", notes = "Удаляет блюдо с идентификатором id.")
    public ResponseEntity<String> delete(@PathVariable("id") Long id) {
        nutritionService.delete(id);
        return ResponseEntity.ok().build();
    }
}
