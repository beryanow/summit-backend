package ru.summit.controller;

import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.summit.entity.dto.GoalDto;
import ru.summit.service.GoalService;

import java.util.Collection;

@RestController
public class GoalsController {

    private final GoalService goalService;

    @Autowired
    public GoalsController(GoalService goalService) {
        this.goalService = goalService;
    }

    @GetMapping("/api/goal/getAll")
    @ApiOperation(value = "Получить список целей", notes = "Возвращает список объектов целей. " +
            "Цель содержит в себе список задач.")
    public ResponseEntity<Collection<GoalDto>> getAll() {
        return ResponseEntity.ok(goalService.getAll());
    }

    @PutMapping("/api/goal")
    @ApiOperation(value = "Создать или изменить цель", notes = "Создает новую цель. " +
            "Если цель с таким id уже есть, то изменяет существующую.\n" +
            "Поле Stage у заданий может принимать значения: new, in progress, done\n" +
            "Название цели не может превышать 100 символов.\n" +
            "Описание цели не может превышать 1000 символов.\n" +
            "Название задания не может превышать 100 символов.\n" +
            "Описание задания не может превышать 500 символов.")
    public ResponseEntity<GoalDto> create(@RequestBody GoalDto goalDto) {
        return ResponseEntity.ok(goalService.createOrChange(goalDto));
    }

    @DeleteMapping("/api/goal/delete/{id}")
    @ApiOperation(value = "Удалить цель", notes = "Удаляет цель с указанным id.")
    public ResponseEntity<String> deleteById(@PathVariable("id") Long id) {
        goalService.deleteById(id);
        return ResponseEntity.ok().build();
    }
}
