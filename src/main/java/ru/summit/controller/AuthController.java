package ru.summit.controller;

import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import ru.summit.entity.dto.UserDto;
import ru.summit.exception.ServiceException;
import ru.summit.service.UserDetailsServiceImpl;

import static ru.summit.util.SecurityConstants.SIGN_UP_URL;

@RestController
public class AuthController {
    private final BCryptPasswordEncoder bCryptPasswordEncoder;
    private final UserDetailsServiceImpl userDetailsService;

    @Autowired
    public AuthController(BCryptPasswordEncoder bCryptPasswordEncoder, UserDetailsServiceImpl userDetailsService) {
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
        this.userDetailsService = userDetailsService;
    }

    @PostMapping(SIGN_UP_URL)
    @ApiOperation(value = "Регистрация", notes = "Регистрация пользователя с указанными данными.\n" +
            "Имя пользователя не может превышать 100 символов.\n" +
            "Имя не может превышать 100 символов.\n" +
            "Фамилия не может превышать 100 символов.\n" +
            "Адрес почты не может превышать 300 символов.(deprecated)\n\n" +
            "Авторизация проивзодится с помощью запроса\n POST /login\n" +
            "{\n" +
            "\"username\": \"username\",\n" +
            "\"password\": \"qwerty\"\n" +
            "}\n" +
            "В ответ на данный запрос в случаи успеха вам вернется 200 и заголовок Authorization, в котором будет содержать строка вида \"Bearer token\", где вместо token будет ваш ключ для подтверждения своей личности.\n" +
            "В дальнейшем, при попытке соврешить любой следующий запрос, необходимо добавить заголовок\n" +
            "Authorization : Bearer token")
    public Long signUp(@RequestBody UserDto userDto) {
        if (userDto.getPassword() == null) {
            throw new ServiceException("Пароль не может отсутствовать.");
        }
        if (userDto.getPassword().isEmpty()) {
            throw new ServiceException("Пароль не может быть пустым.");
        }
        userDto.setPassword(bCryptPasswordEncoder.encode(userDto.getPassword()));
        return userDetailsService.save(userDto);
    }
}
