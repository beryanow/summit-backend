package ru.summit.controller;

import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import ru.summit.entity.dto.ImageDto;
import ru.summit.service.ImageService;

import javax.websocket.server.PathParam;

@RestController
public class ImageController {
    private final ImageService imageService;

    @Autowired
    public ImageController(ImageService imageService) {
        this.imageService = imageService;
    }

    @GetMapping("/api/image")
    @ApiOperation(value = "Получить изображение", notes = "Возвращает изображение по указанному id. Тело " +
            "изображения(поле content) закодирован в формате Base64.")
    public ResponseEntity<ImageDto> getById(@PathParam("id") Long id) {
        final ImageDto dto = imageService.getById(id);
        if (dto == null) {
            return ResponseEntity.notFound().build();
        } else {
            return ResponseEntity.ok(dto);
        }
    }

    @PostMapping("/api/image")
    @ApiOperation(value = "Создать изображение", notes = "Сохраняет изображение на сервер. Тело " +
            "изображения(поле content) должен быть закодирован в формате Base64. " +
            "Размер изображения не должен превышать 2МБ." +
            "Возвращает изображение с актуальной id.")
    public ResponseEntity<ImageDto> save(@RequestBody ImageDto imageDto) {
        return ResponseEntity.ok(imageService.save(imageDto));
    }
}