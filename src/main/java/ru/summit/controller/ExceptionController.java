package ru.summit.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import ru.summit.exception.ServiceException;

@ControllerAdvice
public class ExceptionController {
    @ExceptionHandler(ServiceException.class)
    public ResponseEntity<String> httpError(ServiceException serviceException) {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(serviceException.getMessage());
    }
}
