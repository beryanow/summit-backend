package ru.summit.controller;

import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.summit.entity.dto.IdeaDto;
import ru.summit.service.IdeaService;

import java.util.Collection;

@RestController
public class IdeasController {
    private final IdeaService ideaService;

    @Autowired
    public IdeasController(IdeaService ideaService) {
        this.ideaService = ideaService;
    }

    @GetMapping("/api/idea/getAll")
    @ApiOperation(value = "Получить список мыслей", notes = "Возвращает список объектов идей.")
    public ResponseEntity<Collection<IdeaDto>> getAll() {
        return ResponseEntity.ok(ideaService.getAll());
    }

    @PutMapping("/api/idea")
    @ApiOperation(value = "Создать мысль", notes = "Создает новую идею. " +
            "Если мысль с таким id уже есть, то изменяет существующую.\n" +
            "Название идеи не может превышать 100 символов.\n" +
            "Описание идеи не может превышать 1000 символов.\n")
    public ResponseEntity<IdeaDto> create(@RequestBody IdeaDto ideaDto) {
        return ResponseEntity.ok(ideaService.create(ideaDto));
    }

    @DeleteMapping("/api/ideas/delete/{id}")
    @ApiOperation(value = "Удалить мысль", notes = "Удаляет идею с идентификатором id.")
    public ResponseEntity<String> delete(@PathVariable("id") Long id) {
        ideaService.deleteById(id);
        return ResponseEntity.ok().build();
    }

}
