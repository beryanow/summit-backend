package ru.summit.controller;

import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.summit.entity.dto.ImageDto;
import ru.summit.entity.dto.ProfileDto;
import ru.summit.entity.dto.ProgressDto;
import ru.summit.service.ProfileService;

import javax.websocket.server.PathParam;
import java.util.Collection;

@RestController
public class ProfileController {
    private final ProfileService profileService;

    @Autowired
    public ProfileController(ProfileService profileService) {
        this.profileService = profileService;
    }

    @GetMapping("/api/profile")
    @ApiOperation(value = "Получить характеристики профиля",
            notes = "Возвращает объект профиля с основной информацией о пользователе.")
    public ResponseEntity<ProfileDto> getProfile() {
        return ResponseEntity.ok(profileService.getProfile());
    }

    @PutMapping("/api/profile")
    @ApiOperation(value = "Изменить характеристики профиля",
            notes = "Передаёт объект профиля с новой информацией перезаписывая. \n" +
                    "Имя пользователя не может превышать 100 символов.\n" +
                    "Фамилия пользователя не может превышать 100 символов.\n" +
                    "Адрес почты пользователя не может превышать 300 символов.")
    public ResponseEntity<ProfileDto> changeProfile(@RequestBody ProfileDto profileDto) {
        return ResponseEntity.ok(profileService.changeProfile(profileDto));
    }

    @GetMapping("/api/profile/progress")
    @ApiOperation(value = "Получить список прогрессов до указанной даты",
            notes = "Возвращает объект со списком изображений прогресса обязательно от даты date. " +
                    "Если параметр date не указан, возвращает все прогрессы.\n" +
                    "Формат даты ISO-8601 (2020-12-31)")
    public ResponseEntity<Collection<ProgressDto>> getProgressByDate(@PathParam("date") String date) {
        return ResponseEntity.ok(profileService.getProgressByDate(date));
    }

    @PostMapping("/api/profile/progress")
    @ApiOperation(value = "Создать прогресс",
            notes = "Создает прогресс с указанной датой. Если не указано изображение, " +
                    "будет выбрано стандартное значение.")
    public ResponseEntity<ProgressDto> createProgress(@RequestBody ProgressDto progressDto) {
        return ResponseEntity.ok(profileService.createProgress(progressDto));
    }

    @DeleteMapping("/api/profile/progress/delete/{id}")
    @ApiOperation(value = "Удалить прогресс",
            notes = "Удаляет изображение прогресса по идентификатору.")
    public ResponseEntity<String> deleteProgressById(@PathVariable("id") Long id) {
        profileService.deleteProgressById(id);
        return ResponseEntity.ok().build();
    }

    @DeleteMapping("/api/profile/progress/deleteAll")
    @ApiOperation(value = "Удалить все прогрессы", notes = "Удаляет все прогрессы пользователя.")
    public ResponseEntity<String> deleteAllProgresses() {
        profileService.deleteAllProgresses();
        return ResponseEntity.ok().build();
    }

    @PostMapping("/api/profile/setGoalsBackground")
    @ApiOperation(value = "Загрузить изображение фона целей",
            notes = "Принимает изображение экрана с целями и записывает в таблицу Settings.")
    public ResponseEntity<ImageDto> setGoalsImage(@RequestBody ImageDto imageDto) {
        return ResponseEntity.ok(profileService.setGoalsImage(imageDto));
    }

    @PostMapping("/api/profile/setIdeasBackground")
    @ApiOperation(value = "Загрузить изображение фона идей",
            notes = "Принимает изображение экрана с мыслями и записывает в таблицу Settings.")
    public ResponseEntity<ImageDto> setIdeasImage(@RequestBody ImageDto imageDto) {
        return ResponseEntity.ok(profileService.setIdeasImage(imageDto));
    }

    @PostMapping("/api/profile/setNutritionBackground")
    @ApiOperation(value = "Загрузить изображение фона блюд",
            notes = "Принимает изображение экрана с питанием и записывает в таблицу Settings.")
    public ResponseEntity<ImageDto> setNutritionImage(@RequestBody ImageDto imageDto) {
        return ResponseEntity.ok(profileService.setNutritionImage(imageDto));
    }

    @PostMapping("/api/profile/setTrainingBackground")
    @ApiOperation(value = "Загрузить изображение фона тренировок",
            notes = "Принимает изображение экрана с тренировками и записывает в таблицу Settings.")
    public ResponseEntity<ImageDto> setTrainingImage(@RequestBody ImageDto imageDto) {
        return ResponseEntity.ok(profileService.setTrainingImage(imageDto));
    }

    @PostMapping("/api/profile/setProfileBackground")
    @ApiOperation(value = "Загрузить изображение фона профиля",
            notes = "Принимает изображение экрана с настройками профиля и записывает в таблицу Settings.")
    public ResponseEntity<ImageDto> setProfileImage(@RequestBody ImageDto imageDto) {
        return ResponseEntity.ok(profileService.setProfileBackgroundImage(imageDto));
    }
}
