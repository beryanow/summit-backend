package ru.summit.controller;

import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.summit.entity.core.Type;
import ru.summit.entity.dto.ExerciseSpecsDto;
import ru.summit.entity.dto.TrainingDto;
import ru.summit.exception.ServiceException;
import ru.summit.service.ExerciseSpecsService;
import ru.summit.service.TrainingService;

import javax.websocket.server.PathParam;
import java.util.Collection;

@RestController
public class TrainingController {
    private final TrainingService trainingService;
    private final ExerciseSpecsService exerciseSpecsService;

    @Autowired
    public TrainingController(TrainingService trainingService, ExerciseSpecsService exerciseSpecsService) {
        this.trainingService = trainingService;
        this.exerciseSpecsService = exerciseSpecsService;
    }

    @GetMapping("/api/exercise/getAll")
    @ApiOperation(value = "Получить список упражнений", notes = "Возвращает список объектов упражнений дефолтных " +
            "тренировок.")
    public ResponseEntity<Collection<ExerciseSpecsDto>> getAllExercises() {
        return ResponseEntity.ok(exerciseSpecsService.getAll());
    }

    @GetMapping("/api/training/default/{type}")
    @ApiOperation(value = "Дефолтная тренировка",
            notes = "Возвращает набор дефолтных тренировок указанного типа. " +
                    "Определяется для указанного type: улица, зал. " +
                    "Метод принимает тип как переменную пути.")
    public ResponseEntity<TrainingDto> getDefaultTraining(@PathVariable String type) {
        if (type == null) {
            throw new ServiceException("Тип не может отсутствовать у запроса.");
        }
        return ResponseEntity.ok(trainingService.getDefaultTrainingByType(Type.byName(type)));
    }

    @GetMapping("/api/training/default")
    @ApiOperation(value = "Дефолтная тренировка",
            notes = "Возвращает набор дефолтных тренировок указанного типа. " +
                    "Определяется для указанного type: улица, зал." +
                    "Метод принимает тип как параметр запроса.")
    public ResponseEntity<TrainingDto> getDefaultTrainingPathParam(@PathParam("type") String type) {
        if (type == null) {
            throw new ServiceException("Тип не может отсутствовать у запроса.");
        }
        return ResponseEntity.ok(trainingService.getDefaultTrainingByType(Type.byName(type)));
    }

    @PutMapping("/api/training")
    @ApiOperation(value = "Создать или изменить тренировку",
            notes = "Создает или изменяет тренировку. Чтобы автоматически сгенерировать расписание, поле " +
                    "trainingSchedules должно быть null. Поле cycle определяет сложность. Поддерживаются " +
                    "типы: легкий, средний, сложный, персональный. \nПоля trainingId, userId можно полностью" +
                    " игнорировать.\n" +
                    "Имя тренировки не может превышать 100 символов. \n" +
                    "Имя упражнения не может превышать 100 символов. \n" +
                    "Описание упражнения не может превышать 500 символов. ")
    public ResponseEntity<TrainingDto> createOrChangeTraining(@RequestBody TrainingDto trainingDto) {
        return ResponseEntity.ok(trainingService.createOrChange(trainingDto));
    }

    @DeleteMapping("/api/training/delete/{id}")
    @ApiOperation(value = "Удалить тренировку", notes = "Удаляет тренировку с идентификатором id и все связанные с " +
            "ней упражнения и расписания.")
    public ResponseEntity<String> deleteTraining(@PathVariable("id") Long id) {
        trainingService.delete(id);
        return ResponseEntity.ok().build();
    }

    @GetMapping("/api/trainings")
    @ApiOperation(value = "Получить список тренировок",
            notes = "Возвращает список объектов всех тренировок пользователя.")
    public ResponseEntity<Collection<TrainingDto>> getAllTrainings() {
        return ResponseEntity.ok(trainingService.getAll());
    }

    @GetMapping("/api/trainings/history")
    @ApiOperation(value = "Получить историю тренировок",
            notes = "Получает список объектов всех прошедших тренировок пользователя.")
    public ResponseEntity<Collection<TrainingDto>> getHistory() {
        return ResponseEntity.ok(trainingService.getHistory());
    }
}
