package ru.summit.util;

import lombok.experimental.UtilityClass;
import ru.summit.exception.ServiceException;

import java.time.LocalDate;
import java.time.format.DateTimeParseException;

@UtilityClass
public class DateUtil {
    public static void validateDate(String date) throws ServiceException {
        try {
            parseDate(date);
        } catch (DateTimeParseException e) {
            throw new ServiceException("Неверный формат даты.");
        }
    }

    public static LocalDate parseDate(String date) {
        return LocalDate.parse(date);
    }
}
