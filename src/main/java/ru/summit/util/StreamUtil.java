package ru.summit.util;

import lombok.experimental.UtilityClass;

import javax.validation.constraints.NotNull;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

@UtilityClass
public class StreamUtil {
    public static <T> Stream<T> stream(@NotNull Iterable<T> iterable) {
        return StreamSupport.stream(iterable.spliterator(), false);
    }
}
