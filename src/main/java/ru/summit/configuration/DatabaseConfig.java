package ru.summit.configuration;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;
import java.sql.SQLException;
import java.sql.Statement;

@Slf4j
@Configuration("databaseConfig")
public class DatabaseConfig {
    private final DataSource dataSource;

    @Autowired
    public DatabaseConfig(DataSource dataSource) {
        this.dataSource = dataSource;
        initTables();
    }

    public void initTables() {
        try (
                Statement statement = dataSource.getConnection().createStatement();
        ) {
            initUser(statement);
            initImage(statement);
            initProfile(statement);
            initIdea(statement);
            initDish(statement);
            initIngredient(statement);
            initProgress(statement);
            initSettings(statement);
            initGoal(statement);
            initTask(statement);
            initTraining(statement);
            initExercise(statement);
            initExerciseSpecs(statement);
            initTrainingSchedule(statement);
        } catch (SQLException e) {
            log.error("Error occurred while initializing tables.", e);
        }

    }

    private void initUser(Statement statement) throws SQLException {
        String sql = "CREATE TABLE IF NOT EXISTS USERS(" +
                "ID BIGSERIAL PRIMARY KEY," +
                "USERNAME VARCHAR(100)," +
                "PASSWORD TEXT" +
                ")";
        statement.execute(sql);
    }

    private void initIdea(Statement statement) throws SQLException {
        String sql = "CREATE TABLE IF NOT EXISTS IDEA(" +
                "ID BIGSERIAL PRIMARY KEY," +
                "NAME VARCHAR(100)," +
                "CONTENT VARCHAR(1000)," +
                "BACKGROUND_ID BIGSERIAL," +
                "USER_ID BIGSERIAL," +
                "FOREIGN KEY (USER_ID) REFERENCES USERS(ID)," +
                "FOREIGN KEY (BACKGROUND_ID) REFERENCES IMAGE(ID)," +
                "SIZE VARCHAR(3)" +
                ")";
        statement.execute(sql);
    }

    private void initIngredient(Statement statement) throws SQLException {
        String sql = "CREATE TABLE IF NOT EXISTS INGREDIENT(" +
                "ID BIGSERIAL PRIMARY KEY," +
                "DISH_ID BIGSERIAL," +
                "NAME VARCHAR(100)," +
                "AMOUNT VARCHAR(100)," +
                "USER_ID BIGSERIAL," +
                "FOREIGN KEY (USER_ID) REFERENCES USERS(ID)," +
                "FOREIGN KEY (DISH_ID) REFERENCES DISH(ID)" +
                ")";
        statement.execute(sql);
    }

    private void initDish(Statement statement) throws SQLException {
        String sql = "CREATE TABLE IF NOT EXISTS DISH(" +
                "ID BIGSERIAL PRIMARY KEY," +
                "NAME VARCHAR(100)," +
                "DESCRIPTION VARCHAR(500)," +
                "RECIPE TEXT," +
                "BACKGROUND_ID BIGSERIAL," +
                "USER_ID BIGSERIAL," +
                "FOREIGN KEY (USER_ID) REFERENCES USERS(ID)," +
                "FOREIGN KEY (BACKGROUND_ID) REFERENCES IMAGE(ID)," +
                "SIZE VARCHAR(3)" +
                ")";
        statement.execute(sql);
    }

    private void initImage(Statement statement) throws SQLException {
        String sql = "CREATE TABLE IF NOT EXISTS IMAGE(" +
                "ID BIGSERIAL PRIMARY KEY," +
                "CONTENT BYTEA" +
                ")";
        statement.execute(sql);
    }

    private void initProfile(Statement statement) throws SQLException {
        String sql = "CREATE TABLE IF NOT EXISTS PROFILE(" +
                "ID BIGSERIAL PRIMARY KEY," +
                "NAME VARCHAR(100)," +
                "SURNAME VARCHAR(100)," +
                "AGE INTEGER," +
                "IMAGE_ID BIGSERIAL," +
                "EMAIL VARCHAR(300)," +
                "USER_ID BIGSERIAL," +
                "FOREIGN KEY (IMAGE_ID) REFERENCES IMAGE(ID)," +
                "FOREIGN KEY (USER_ID) REFERENCES USERS(ID)" +
                ")";
        statement.execute(sql);
    }

    private void initProgress(Statement statement) throws SQLException {
        String sql = "CREATE TABLE IF NOT EXISTS PROGRESS(" +
                "ID BIGSERIAL PRIMARY KEY," +
                "PROFILE_ID BIGSERIAL," +
                "IMAGE_ID BIGSERIAL," +
                "DATE VARCHAR(20)," +
                "FOREIGN KEY (IMAGE_ID) REFERENCES IMAGE(ID)," +
                "FOREIGN KEY (PROFILE_ID) REFERENCES PROFILE(ID)" +
                ")";
        statement.execute(sql);
    }

    private void initGoal(Statement statement) throws SQLException {
        String sql = "CREATE TABLE IF NOT EXISTS GOAL(" +
                "ID BIGSERIAL PRIMARY KEY," +
                "NAME VARCHAR(100)," +
                "DESCRIPTION VARCHAR(1000)," +
                "BACKGROUND_ID BIGSERIAL," +
                "SIZE VARCHAR(3)," +
                "USER_ID BIGSERIAL," +
                "FOREIGN KEY (USER_ID) REFERENCES USERS(ID)," +
                "STATE_PLACE VARCHAR(100)," +
                "FOREIGN KEY (BACKGROUND_ID) REFERENCES IMAGE(ID)" +
                ")";
        statement.execute(sql);
    }

    private void initTask(Statement statement) throws SQLException {
        String sql = "CREATE TABLE IF NOT EXISTS TASK(" +
                "ID BIGSERIAL PRIMARY KEY," +
                "GOAL_ID BIGSERIAL," +
                "NAME VARCHAR(100)," +
                "DESCRIPTION VARCHAR(500)," +
                "STAGE VARCHAR(100)," +
                "USER_ID BIGSERIAL," +
                "FOREIGN KEY (USER_ID) REFERENCES USERS(ID)," +
                "FOREIGN KEY (GOAL_ID) REFERENCES GOAL(ID)" +
                ")";
        statement.execute(sql);
    }

    private void initSettings(Statement statement) throws SQLException {
        String sql = "CREATE TABLE IF NOT EXISTS SETTINGS(" +
                "ID BIGSERIAL PRIMARY KEY," +
                "PROFILE_ID BIGSERIAL," +
                "GOALS_IMAGE_ID BIGSERIAL," +
                "IDEAS_IMAGE_ID BIGSERIAL," +
                "NUTRITION_IMAGE_ID BIGSERIAL," +
                "TRAINING_IMAGE_ID BIGSERIAL," +
                "PROFILE_IMAGE_ID BIGSERIAL," +
                "WELCOME_IMAGE_ID BIGSERIAL," +
                "FOREIGN KEY (PROFILE_ID) REFERENCES PROFILE(ID)," +
                "FOREIGN KEY (GOALS_IMAGE_ID) REFERENCES IMAGE(ID)," +
                "FOREIGN KEY (IDEAS_IMAGE_ID) REFERENCES IMAGE(ID)," +
                "FOREIGN KEY (NUTRITION_IMAGE_ID) REFERENCES IMAGE(ID)," +
                "FOREIGN KEY (TRAINING_IMAGE_ID) REFERENCES IMAGE(ID)," +
                "FOREIGN KEY (PROFILE_IMAGE_ID) REFERENCES IMAGE(ID)," +
                "FOREIGN KEY (WELCOME_IMAGE_ID) REFERENCES IMAGE(ID)" +
                ")";
        statement.execute(sql);
    }

    private void initExercise(Statement statement) throws SQLException {
        String sql = "CREATE TABLE IF NOT EXISTS EXERCISE(" +
                "ID BIGSERIAL PRIMARY KEY," +
                "NAME VARCHAR(100)," +
                "DESCRIPTION VARCHAR(500)," +
                "TYPE VARCHAR(50)," +
                "REPETITIONS BIGSERIAL," +
                "TRAINING_ID BIGSERIAL," +
                "FOREIGN KEY (TRAINING_ID) REFERENCES TRAINING(ID)" +
                ")";
        statement.execute(sql);
    }

    private void initTraining(Statement statement) throws SQLException {
        String sql = "CREATE TABLE IF NOT EXISTS TRAINING(" +
                "ID BIGSERIAL PRIMARY KEY," +
                "USER_ID BIGSERIAL," +
                "NAME VARCHAR(100)," +
                "CYCLE VARCHAR(50)," +
                "FOREIGN KEY (USER_ID) REFERENCES USERS(ID)" +
                ")";
        statement.execute(sql);
    }

    private void initTrainingSchedule(Statement statement) throws SQLException {
        String sql = "CREATE TABLE IF NOT EXISTS TRAINING_SCHEDULE(" +
                "ID BIGSERIAL PRIMARY KEY," +
                "TRAINING_ID BIGSERIAL," +
                "DATE VARCHAR(50)," +
                "FOREIGN KEY (TRAINING_ID) REFERENCES TRAINING(ID)" +
                ")";
        statement.execute(sql);
    }

    private void initExerciseSpecs(Statement statement) throws SQLException {
        String sql = "CREATE TABLE IF NOT EXISTS EXERCISE_SPECS(" +
                "ID BIGSERIAL PRIMARY KEY," +
                "NAME VARCHAR(100)," +
                "DESCRIPTION VARCHAR(500)," +
                "TYPE VARCHAR(50)," +
                "REPETITIONS BIGSERIAL" +
                ")";
        statement.execute(sql);
    }
}
