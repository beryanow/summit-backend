package ru.summit.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;
import ru.summit.entity.db.Task;

public interface TaskRepository extends CrudRepository<Task, Long> {
    @Override
    Iterable<Task> findAll();

    @Override
    <S extends Task> S save(S entity);

    @Override
    void deleteById(Long aLong);

    @Transactional
    Iterable<Task> findAllByGoalIdAndUserId(Long id, Long authorizedUserId);

    @Transactional
    void deleteAllByGoalIdAndUserId(Long id, Long authorizedUserId);
}
