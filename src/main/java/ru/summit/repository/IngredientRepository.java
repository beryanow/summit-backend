package ru.summit.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;
import ru.summit.entity.db.Ingredient;

public interface IngredientRepository extends CrudRepository<Ingredient, Long> {
    @Override
    <S extends Ingredient> S save(S entity);

    Iterable<Ingredient> findAllByDishIdAndUserId(Long id, Long authorizedUserId);

    @Transactional
    void deleteAllByDishIdAndUserId(Long id, Long authorizedUserId);
}
