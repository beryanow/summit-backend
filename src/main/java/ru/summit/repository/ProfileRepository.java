package ru.summit.repository;

import org.springframework.data.repository.CrudRepository;
import ru.summit.entity.db.Profile;

import java.util.Optional;

public interface ProfileRepository extends CrudRepository<Profile, Long> {

    Profile findByUserId(Long userId);

    @Override
    Optional<Profile> findById(Long aLong);

    @Override
    Iterable<Profile> findAll();

    @Override
    <S extends Profile> S save(S entity);

    @Override
    void deleteById(Long aLong);
}
