package ru.summit.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;
import ru.summit.entity.db.Idea;

public interface IdeaRepository extends CrudRepository<Idea, Long> {
    @Override
    <S extends Idea> S save(S entity);

    Iterable<Idea> findAllByUserId(Long authorizedUserId);

    @Transactional
    void deleteByIdAndUserId(Long id, Long authorizedUserId);
}
