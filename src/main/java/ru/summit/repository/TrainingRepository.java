package ru.summit.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;
import ru.summit.entity.db.Training;

public interface TrainingRepository extends CrudRepository<Training, Long> {
    Iterable<Training> findAllByUserId(Long authorizedUserId);

    boolean existsByIdAndUserId(Long id, Long authorizedUserId);

    @Transactional
    void deleteByIdAndUserId(Long dishId, Long authorizedUserId);

    @Override
    <S extends Training> S save(S entity);
}
