package ru.summit.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;
import ru.summit.entity.db.Dish;

public interface DishRepository extends CrudRepository<Dish, Long> {
    @Override
    <S extends Dish> S save(S entity);

    Iterable<Dish> findAllByUserId(Long authorizedUserId);

    boolean existsByIdAndUserId(Long id, Long authorizedUserId);

    @Transactional
    void deleteByIdAndUserId(Long dishId, Long authorizedUserId);
}
