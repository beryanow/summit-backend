package ru.summit.repository;

import org.springframework.data.repository.CrudRepository;
import ru.summit.entity.db.ExerciseSpecs;

public interface ExerciseSpecsRepository extends CrudRepository<ExerciseSpecs, Long> {
    @Override
    <S extends ExerciseSpecs> S save(S entity);

    Iterable<ExerciseSpecs> findAllByType(String type);
}
