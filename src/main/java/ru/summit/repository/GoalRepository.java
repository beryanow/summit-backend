package ru.summit.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;
import ru.summit.entity.db.Goal;

public interface GoalRepository extends CrudRepository<Goal, Long> {
    Iterable<Goal> findAllByUserId(Long authorizedUserId);

    boolean existsByIdAndUserId(Long id, Long userId);

    @Override
    <S extends Goal> S save(S entity);

    @Transactional
    void deleteByIdAndUserId(Long id, Long userId);
}
