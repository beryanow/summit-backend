package ru.summit.repository;

import org.springframework.data.repository.CrudRepository;
import ru.summit.entity.db.Settings;

public interface SettingsRepository extends CrudRepository<Settings, Long> {
    @Override
    Iterable<Settings> findAll();

    @Override
    <S extends Settings> S save(S entity);

    Settings findByProfileId(Long profileId);

    @Override
    void deleteById(Long aLong);
}
