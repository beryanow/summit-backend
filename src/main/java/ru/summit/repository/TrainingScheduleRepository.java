package ru.summit.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;
import ru.summit.entity.db.Exercise;
import ru.summit.entity.db.TrainingSchedule;

public interface TrainingScheduleRepository extends CrudRepository<TrainingSchedule, Long> {
    @Override
    <S extends TrainingSchedule> S save(S entity);

    @Transactional
    void deleteAllByTrainingId(Long id);

    @Transactional
    Iterable<TrainingSchedule> findAllByTrainingId(Long id);
}
