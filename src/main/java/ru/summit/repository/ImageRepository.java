package ru.summit.repository;

import org.springframework.data.repository.CrudRepository;
import ru.summit.entity.db.Image;

import java.util.Optional;

public interface ImageRepository extends CrudRepository<Image, Long> {
    @Override
    Optional<Image> findById(Long aLong);

    @Override
    <S extends Image> S save(S entity);

    @Override
    void deleteById(Long aLong);

    @Override
    boolean existsById(Long aLong);
}
