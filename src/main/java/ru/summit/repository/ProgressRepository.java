package ru.summit.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;
import ru.summit.entity.db.Progress;

public interface ProgressRepository extends CrudRepository<Progress, Long> {
    @Override
    <S extends Progress> S save(S entity);

    @Override
    void deleteById(Long aLong);

    @Transactional
    void deleteAllByProfileId(Long id);

    Iterable<Progress> findAllByProfileId(Long id);
}
