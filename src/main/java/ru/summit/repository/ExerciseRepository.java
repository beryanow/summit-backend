package ru.summit.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;
import ru.summit.entity.db.Exercise;

public interface ExerciseRepository extends CrudRepository<Exercise, Long> {
    @Override
    <S extends Exercise> S save(S entity);

    @Transactional
    void deleteAllByTrainingId(Long id);

    @Transactional
    Iterable<Exercise> findAllByTrainingId(Long id);
}
