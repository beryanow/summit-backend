package ru.summit.repository;

import org.springframework.data.repository.CrudRepository;
import ru.summit.entity.db.ApplicationUser;

import java.util.Optional;

public interface UserRepository extends CrudRepository<ApplicationUser, Long> {
    Optional<ApplicationUser> findByUsername(String aLong);

    boolean existsByUsername(String username);
}
