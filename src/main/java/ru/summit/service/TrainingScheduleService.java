package ru.summit.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.summit.entity.db.TrainingSchedule;
import ru.summit.entity.dto.TrainingScheduleDto;
import ru.summit.exception.ServiceException;
import ru.summit.repository.TrainingScheduleRepository;
import ru.summit.util.DateUtil;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import static ru.summit.util.StreamUtil.stream;

@Slf4j
@Service
public class TrainingScheduleService {
    private static final long TRAINING_DAYS = 30;

    private final TrainingScheduleRepository trainingScheduleRepository;

    @Autowired
    public TrainingScheduleService(TrainingScheduleRepository trainingScheduleRepository) {
        this.trainingScheduleRepository = trainingScheduleRepository;
    }

    public Collection<TrainingScheduleDto> getAll() {
        return stream(trainingScheduleRepository.findAll())
                .map(TrainingSchedule::dto)
                .collect(Collectors.toList());
    }

    public Collection<TrainingScheduleDto> findAllByTrainingId(Long trainingId) {
        return stream(trainingScheduleRepository.findAllByTrainingId(trainingId))
                .map(TrainingSchedule::dto)
                .collect(Collectors.toList());
    }

    public Collection<TrainingScheduleDto> getHistory(Long trainingId) {
        return stream(trainingScheduleRepository.findAllByTrainingId(trainingId))
                .filter(trainingSchedule -> DateUtil.parseDate(trainingSchedule.getDate()).isBefore(LocalDate.now()))
                .map(TrainingSchedule::dto)
                .collect(Collectors.toList());
    }

    public void deleteAllByTrainingId(Long id) {
        trainingScheduleRepository.deleteAllByTrainingId(id);
    }

    public Iterable<TrainingSchedule> saveAll(Iterable<TrainingSchedule> trainingSchedules) {
        return trainingScheduleRepository.saveAll(trainingSchedules);
    }

    public Iterable<TrainingSchedule> create(Long trainingId) {
        List<TrainingSchedule> trainingSchedules = new ArrayList<>();
        LocalDate startTraining = LocalDate.now();
        long trainingDays = 0;
        while (trainingDays <= TRAINING_DAYS) {
            final TrainingSchedule trainingSchedule = new TrainingSchedule();
            trainingSchedule.setTrainingId(trainingId);
            trainingSchedule.setDate(startTraining.plusDays(trainingDays).toString());
            trainingScheduleRepository.save(trainingSchedule);
            trainingSchedules.add(trainingSchedule);
            trainingDays += 3;
        }
        return trainingSchedules;
    }

    public void validateTrainingSchedules(Collection<TrainingScheduleDto> trainingScheduleDtos) {
        if (trainingScheduleDtos == null) {
            return;
        }
        for (TrainingScheduleDto dto : trainingScheduleDtos) {
            if (dto.getTrainingId() == null) {
                throw new ServiceException("Расписание тренировки не может не принадлежать какой-либо конкретной тренировке.");
            }
            if (dto.getDate().isEmpty()) {
                throw new ServiceException("Дата расписания тренировки не может быть пустой.");
            }
            if (dto.getDate() != null) {
                DateUtil.validateDate(dto.getDate());
            }
        }
    }
}
