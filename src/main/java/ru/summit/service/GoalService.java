package ru.summit.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Service;
import ru.summit.entity.core.Stage;
import ru.summit.entity.db.Goal;
import ru.summit.entity.db.Task;
import ru.summit.entity.dto.GoalDto;
import ru.summit.entity.dto.TaskDto;
import ru.summit.exception.ServiceException;
import ru.summit.repository.GoalRepository;
import ru.summit.repository.ImageRepository;
import ru.summit.repository.TaskRepository;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import static ru.summit.entity.core.Stage.byName;
import static ru.summit.util.StreamUtil.stream;

@Service
@DependsOn("ImageService")
@Slf4j
public class GoalService extends AuthorizedUserService {

    private final GoalRepository goalRepository;
    private final TaskRepository taskRepository;
    private final ImageRepository imageRepository;

    @Autowired
    public GoalService(GoalRepository goalRepository, TaskRepository taskRepository, ImageRepository imageRepository) {
        this.goalRepository = goalRepository;
        this.taskRepository = taskRepository;
        this.imageRepository = imageRepository;
    }

    public Collection<GoalDto> getAll() {
        final List<GoalDto> collect = stream(goalRepository.findAllByUserId(getAuthorizedUserId()))
                .map(Goal::dto)
                .collect(Collectors.toList());
        collect.forEach(goalDto -> goalDto.setTasks(getTasksByGoalId(goalDto.getId())));
        return collect;
    }

    private Collection<TaskDto> getTasksByGoalId(Long id) {
        return stream(taskRepository.findAllByGoalIdAndUserId(id, getAuthorizedUserId()))
                .map(Task::dto).collect(Collectors.toList());
    }

    public GoalDto createOrChange(GoalDto goalDto) {
        boolean isNew = goalDto.getId() != null && goalRepository.existsByIdAndUserId(goalDto.getId(), getAuthorizedUserId());
        validateGoal(goalDto);
        validateTasks(goalDto.getTasks());
        if (!imageRepository.existsById(goalDto.getBackgroundId())) {
            goalDto.setBackgroundId(1L);
        }
        if (goalDto.getTasks() != null) {
            stream(goalDto.getTasks()).forEach(task -> {
                final Stage stage = byName(task.getStage());
                task.setStage(stage.toString());
            });
        }
        goalDto.setUserId(getAuthorizedUserId());
        final GoalDto dto = goalRepository.save(goalDto.goal()).dto();
        if (isNew) {
            taskRepository.deleteAllByGoalIdAndUserId(dto.getId(), getAuthorizedUserId());
        }
        final Iterable<Task> tasks = taskRepository.saveAll(goalDto.getTasks() == null ? new ArrayList<>() : goalDto.getTasks()
                .stream()
                .map(taskDto -> {
                    taskDto.setUserId(getAuthorizedUserId());
                    return taskDto.task(dto.getId());
                })
                .collect(Collectors.toList()));
        dto.setTasks(stream(tasks)
                .map(Task::dto)
                .collect(Collectors.toList()));
        return dto;
    }

    private void validateTasks(Collection<TaskDto> tasks) {
        if (tasks != null) {
            for (TaskDto task : tasks) {
                if (task.getName() == null) {
                    throw new ServiceException("Название задания не может отсутствовать.");
                }
                if (task.getDescription() == null) {
                    throw new ServiceException("Описание задания не может отсутствовать.");
                }
                if (task.getDescription().length() > 500) {
                    throw new ServiceException("Текст описания задания не может превышать 500 символов.");
                }
                if (task.getName().length() > 100) {
                    throw new ServiceException("Название задания не может превышать 100 символов.");
                }
                if (task.getName().isEmpty()) {
                    throw new ServiceException("Название задания не может быть пустым.");
                }
                if (byName(task.getStage()) == null) {
                    throw new ServiceException("Неподходящее состояние задания.");
                }
            }
        }
    }

    private void validateGoal(GoalDto goalDto) {
        if (goalDto.getName() == null) {
            throw new ServiceException("Название цели не может отсутствовать.");
        }
        if (goalDto.getDescription() == null) {
            throw new ServiceException("Описание цели не может отсутствовать.");
        }
        if (goalDto.getDescription().length() > 1000) {
            throw new ServiceException("Текст описания цели не может превышать 1000 символов.");
        }
        if (goalDto.getName().isEmpty()) {
            throw new ServiceException("Название цели не может быть пустым.");
        }
        if (goalDto.getName().length() > 100) {
            throw new ServiceException("Название цели не может превышать 100 символов.");
        }
        if (!goalDto.getSize().matches("[12]x[12]")) {
            throw new ServiceException("Размер поля не соответствует требованиям.");
        }
    }

    public void deleteById(Long id) {
        taskRepository.deleteAllByGoalIdAndUserId(id, getAuthorizedUserId());
        goalRepository.deleteByIdAndUserId(id, getAuthorizedUserId());
    }
}
