package ru.summit.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.summit.entity.core.Cycle;
import ru.summit.entity.core.Type;
import ru.summit.entity.db.*;
import ru.summit.entity.dto.ExerciseDto;
import ru.summit.entity.dto.TrainingDto;
import ru.summit.exception.ServiceException;
import ru.summit.repository.TrainingRepository;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import static ru.summit.util.StreamUtil.stream;

@Slf4j
@Service
@Transactional
public class TrainingService extends AuthorizedUserService {
    private static final int MEDIUM_REPETITIONS = 10;
    private static final int HARD_REPETITIONS = 15;

    private final TrainingRepository trainingRepository;
    private final ExerciseSpecsService exerciseSpecsService;
    private final ExerciseService exerciseService;
    private final TrainingScheduleService trainingScheduleService;

    @Autowired
    public TrainingService(TrainingRepository trainingRepository, ExerciseSpecsService exerciseSpecsService,
                           ExerciseService exerciseService, TrainingScheduleService trainingScheduleService) {
        this.trainingRepository = trainingRepository;
        this.exerciseSpecsService = exerciseSpecsService;
        this.exerciseService = exerciseService;
        this.trainingScheduleService = trainingScheduleService;
    }

    public Collection<TrainingDto> getAll() {
        List<TrainingDto> collect = stream(trainingRepository.findAllByUserId(getAuthorizedUserId()))
                .map(Training::dto)
                .collect(Collectors.toList());
        collect.forEach(trainingDto -> {
            trainingDto.setExercises(exerciseService.findAllByTrainingId(trainingDto.getId()));
            trainingDto.setTrainingSchedules(trainingScheduleService.findAllByTrainingId(trainingDto.getId()));
        });
        return collect;
    }

    public TrainingDto getDefaultTrainingByType(Type type) {
        if (type == null) {
            throw new ServiceException("неверный тип тренировки.");
        }
        TrainingDto trainingDto = new TrainingDto();
        trainingDto.setName("Стандартная тренировка");
        trainingDto.setCycle(Cycle.LIGHT.toString());
        List<ExerciseDto> exerciseSpecsDtos = exerciseSpecsService.getExercisesByType(type).stream()
                .map(exerciseService::create)
                .collect(Collectors.toList());
        trainingDto.setExercises(exerciseSpecsDtos);
        return trainingDto;
    }

    public Collection<TrainingDto> getHistory() {
        List<TrainingDto> allTrainings = stream(trainingRepository.findAllByUserId(getAuthorizedUserId()))
                .map(Training::dto)
                .collect(Collectors.toList());
        List<TrainingDto> history = new ArrayList<>();
        for (TrainingDto curTrain : allTrainings) {
            if (!trainingScheduleService.getHistory(curTrain.getId()).isEmpty()) {
                curTrain.setTrainingSchedules(trainingScheduleService.getHistory(curTrain.getId()));
                history.add(curTrain);
            }
        }
        return history;
    }

    public TrainingDto createOrChange(TrainingDto trainingDto) {
        boolean isOld = trainingDto.getId() != null && trainingRepository.existsByIdAndUserId(trainingDto.getId(), getAuthorizedUserId());
        validateTraining(trainingDto);
        exerciseService.validateExercises(trainingDto.getExercises(), isOld);
        trainingScheduleService.validateTrainingSchedules(trainingDto.getTrainingSchedules());
        trainingDto.setUserId(getAuthorizedUserId());
        final TrainingDto dto = trainingRepository.save(trainingDto.training()).dto();
        if (isOld) {
            exerciseService.deleteAllByTrainingId(dto.getId());
            trainingScheduleService.deleteAllByTrainingId(dto.getId());
        }
        final Iterable<Exercise> exercises = exerciseService.saveAll(trainingDto.getExercises()
                .stream()
                .map(exerciseDto -> {
                    if (Cycle.byName(trainingDto.getCycle()) == Cycle.MEDIUM) {
                        exerciseDto.setRepetitions(exerciseDto.getRepetitions() + MEDIUM_REPETITIONS);
                    } else if (Cycle.byName(trainingDto.getCycle()) == Cycle.HARD) {
                        exerciseDto.setRepetitions(exerciseDto.getRepetitions() + HARD_REPETITIONS);
                    }
                    return exerciseDto.exercise(dto.getId());
                })
                .collect(Collectors.toList()));
        dto.setExercises(stream(exercises)
                .map(Exercise::dto)
                .collect(Collectors.toList()));
        Iterable<TrainingSchedule> trainingSchedules = null;
        if (trainingDto.getTrainingSchedules() != null) {
            trainingSchedules = trainingScheduleService.saveAll(trainingDto.getTrainingSchedules()
                    .stream()
                    .map(trainingScheduleDto -> trainingScheduleDto.trainingSchedule(dto.getId()))
                    .collect(Collectors.toList()));
        } else {
            trainingSchedules = trainingScheduleService.create(dto.getId());
        }
        dto.setTrainingSchedules(stream(trainingSchedules)
                .map(TrainingSchedule::dto)
                .collect(Collectors.toList()));
        return dto;
    }

    private void validateTraining(TrainingDto trainingDto) {
        if (trainingDto.getName().isEmpty()) {
            throw new ServiceException("Название тренировки не может быть пустым.");
        }
        if (trainingDto.getName().length() > 100) {
            throw new ServiceException("Название тренировки не может превышать 100 символов.");
        }
        if (trainingDto.getCycle() == null) {
            throw new ServiceException("Сложность тренировки должна быть указана.");
        }
        if (Cycle.byName(trainingDto.getCycle()) == null) {
            throw new ServiceException("Неподходящая сложность тренировки.");
        }
    }

    public void delete(Long trainingId) {
        exerciseService.deleteAllByTrainingId(trainingId);
        trainingScheduleService.deleteAllByTrainingId(trainingId);
        trainingRepository.deleteByIdAndUserId(trainingId, getAuthorizedUserId());
    }
}
