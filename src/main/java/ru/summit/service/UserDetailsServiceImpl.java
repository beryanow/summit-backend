package ru.summit.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import ru.summit.entity.core.User;
import ru.summit.entity.db.ApplicationUser;
import ru.summit.entity.dto.ProfileDto;
import ru.summit.entity.dto.UserDto;
import ru.summit.exception.ServiceException;
import ru.summit.repository.UserRepository;

import java.util.Optional;

import static java.util.Collections.emptyList;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    private final UserRepository userRepository;
    private final ProfileService profileService;

    @Autowired
    public UserDetailsServiceImpl(UserRepository userRepository, ProfileService profileService) {
        this.userRepository = userRepository;
        this.profileService = profileService;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        final Optional<ApplicationUser> byUsername = userRepository.findByUsername(username);
        if (!byUsername.isPresent()) {
            throw new UsernameNotFoundException(username);
        }
        final ApplicationUser applicationUser = byUsername.get();
        return new User(applicationUser.getUsername(), applicationUser.getPassword(), emptyList());
    }

    public Long save(UserDto userDto) {
        if (userRepository.existsByUsername(userDto.getUsername())) {
            throw new ServiceException("Пользователь с таким именем уже существует.");
        }
        validate(userDto);
        final ApplicationUser save = userRepository.save(new ApplicationUser(userDto));
        final ProfileDto profileDto = new ProfileDto();
        profileDto.setName(userDto.getName());
        profileDto.setSurname(userDto.getSurname());
        profileDto.setAge(userDto.getAge());
//        profileDto.setEmail(userDto.getEmail());
        profileDto.setUserId(save.getId());
        profileService.createProfile(profileDto);
        return save.getId();
    }

    private void validate(UserDto user) {
        if (user.getAge() <= 0) {
            throw new ServiceException("Возраст не может быть отрицательным.");
        }
//        if (user.getEmail() == null) {
////            throw new ServiceException("Адрес почты не может отсутствовать.");
//        }
        if (user.getName() == null) {
            throw new ServiceException("Имя не может отсутствовать.");
        }
        if (user.getSurname() == null) {
            throw new ServiceException("Фамилия не может отсутствовать.");
        }
//        if (user.getEmail().isEmpty()) {
////            throw new ServiceException("Адрес почты не может быть пустым.");
//        }
        if (user.getSurname().isEmpty()) {
            throw new ServiceException("Фамилия не может быть пустой.");
        }
        if (user.getName().isEmpty()) {
            throw new ServiceException("Имя не может быть пустым.");
        }
        if (user.getPassword().isEmpty()) {
            throw new ServiceException("Пароль не может быть пустым.");
        }
        if (user.getUsername() == null) {
            throw new ServiceException("Имя пользователя не может отсутствовать.");
        }
        if (user.getUsername().isEmpty()) {
            throw new ServiceException("Имя пользователя не может быть пустым.");
        }
        if (user.getUsername().length() > 100) {
            throw new ServiceException("Имя пользователя не может превышать 100 символов.");
        }
        if (user.getName().length() > 100) {
            throw new ServiceException("Имя не может превышать 100 символов.");
        }
        if (user.getSurname().length() > 100) {
            throw new ServiceException("Фамилия не может превышать 100 символов.");
        }
//        if (user.getEmail().length() > 300) {
////            throw new ServiceException("Адрес почты не может превышать 300 символов.");
//        }
    }
}
