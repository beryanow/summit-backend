package ru.summit.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Service;
import ru.summit.entity.db.ExerciseSpecs;
import ru.summit.entity.core.Type;
import ru.summit.entity.dto.ExerciseSpecsDto;
import ru.summit.repository.ExerciseSpecsRepository;

import javax.annotation.PostConstruct;
import java.util.Collection;
import java.util.stream.Collectors;

import static ru.summit.util.StreamUtil.stream;

@Slf4j
@Service
@DependsOn("databaseConfig")
public class ExerciseSpecsService {
    private final ExerciseSpecsRepository exerciseSpecsRepository;

    @Autowired
    public ExerciseSpecsService(ExerciseSpecsRepository exerciseSpecsRepository) {
        this.exerciseSpecsRepository = exerciseSpecsRepository;
    }

    @PostConstruct
    public void init() {
        loadExercise(1L, "Отжимания", "Отжимания на кулаках", Type.GYM.toString(), 10);
        loadExercise(2L, "Приседания", "Приседания", Type.GYM.toString(), 10);
        loadExercise(3L, "Пресс", "Пресс", Type.GYM.toString(), 10);
        loadExercise(4L, "Выпады", "Выпады", Type.GYM.toString(), 10);
        loadExercise(5L, "Бег", "Бег", Type.STREET.toString(), 1);
        loadExercise(6L, "Бег с высоким подъемом коленей", "Бег с высоким подъемом коленей", Type.STREET.toString(), 1);
        loadExercise(7L, "Прыжки", "Прыжки", Type.STREET.toString(), 10);
        loadExercise(8L, "Турник", "Турник", Type.STREET.toString(), 10);
    }

    private void loadExercise(Long id, String name, String description, String type, Integer repetitions) {
        final ExerciseSpecsDto exerciseSpecsDto = new ExerciseSpecsDto();
        exerciseSpecsDto.setId(id);
        exerciseSpecsDto.setName(name);
        exerciseSpecsDto.setDescription(description);
        exerciseSpecsDto.setType(type);
        exerciseSpecsDto.setRepetitions(repetitions);
        log.info("Была сохранена сущность {}", exerciseSpecsRepository.save(exerciseSpecsDto.exerciseSpecs()));
    }

    public Collection<ExerciseSpecsDto> getExercisesByType(Type type) {
        return stream(exerciseSpecsRepository.findAllByType(type.toString()))
                .map(ExerciseSpecs::dto)
                .collect(Collectors.toList());
    }

    public Collection<ExerciseSpecsDto> getAll() {
        return stream(exerciseSpecsRepository.findAll())
                .map(ExerciseSpecs::dto)
                .collect(Collectors.toList());
    }
}
