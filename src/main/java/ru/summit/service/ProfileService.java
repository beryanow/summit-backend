package ru.summit.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Service;
import ru.summit.entity.db.Image;
import ru.summit.entity.db.Profile;
import ru.summit.entity.db.Settings;
import ru.summit.entity.dto.ImageDto;
import ru.summit.entity.dto.ProfileDto;
import ru.summit.entity.dto.ProgressDto;
import ru.summit.entity.dto.SettingsDto;
import ru.summit.exception.ServiceException;
import ru.summit.repository.ImageRepository;
import ru.summit.repository.ProfileRepository;
import ru.summit.repository.ProgressRepository;
import ru.summit.repository.SettingsRepository;
import ru.summit.util.DateUtil;

import java.time.LocalDate;
import java.util.ArrayDeque;
import java.util.Collection;

@Slf4j
@Service
@DependsOn("ImageService")
public class ProfileService extends AuthorizedUserService {

    private final ProfileRepository profileRepository;
    private final ProgressRepository progressRepository;
    private final SettingsRepository settingsRepository;
    private final ImageRepository imageRepository;
    private final ImageService imageService;

    @Autowired
    public ProfileService(ProfileRepository profileRepository,
                          ProgressRepository progressRepository,
                          SettingsRepository settingsRepository,
                          ImageRepository imageRepository, ImageService imageService) {
        super();
        this.profileRepository = profileRepository;
        this.progressRepository = progressRepository;
        this.settingsRepository = settingsRepository;
        this.imageRepository = imageRepository;
        this.imageService = imageService;
    }

    public Collection<ProgressDto> getProgressByDate(String date) {
        LocalDate parse = null;
        if (date != null && !date.isEmpty()) {
            DateUtil.validateDate(date);
            parse = DateUtil.parseDate(date);
        }
        Collection<ProgressDto> progresses = new ArrayDeque<>();
        LocalDate finalParse = parse;
        progressRepository.findAllByProfileId(getProfile().getId()).forEach(progress -> {
            if (finalParse == null) {
                progresses.add(progress.dto());
                return;
            }
            if (DateUtil.parseDate(progress.getDate()).isBefore(finalParse)) {
                progresses.add(progress.dto());
            }
        });
        return progresses;
    }

    public ProgressDto createProgress(ProgressDto progressDto) {
        validateProgressDto(progressDto);
        progressDto.setId(null);
        progressDto.setProfileId(getProfile().getId());
        if (!imageRepository.existsById(progressDto.getImageId())) {
            progressDto.setImageId(2L);
        }
        return progressRepository.save(progressDto.progress()).dto();
    }

    private void validateProgressDto(ProgressDto progressDto) {
        DateUtil.validateDate(progressDto.getDate());
    }


    public ProfileDto getProfile() {
        final Long authorizedUserId = getAuthorizedUserId();
        final Profile profile = profileRepository.findByUserId(authorizedUserId);
        final ProfileDto dto = profile.dto();
        dto.setSettingsDto(settingsRepository.findByProfileId(dto.getId()).dto());
        return dto;
    }

    public ProfileDto createProfile(ProfileDto profileDto) {
        validate(profileDto);
        profileDto.setImageId(3L);
        final Profile save = profileRepository.save(profileDto.profile());
        final SettingsDto settingsDto = defaultSettingDto(save.getId());
        settingsRepository.save(settingsDto.settings());
        return save.dto();
    }

    private void validate(ProfileDto profileDto) {
        if (profileDto.getAge() <= 0) {
            throw new ServiceException("Пароль не может быть пустым.");
        }
//        if (profileDto.getEmail() == null) {
////            throw new ServiceException("Адрес почты не может отсутствовать.");
//        }
        if (profileDto.getName() == null) {
            throw new ServiceException("Имя не может отсутствовать.");
        }
        if (profileDto.getSurname() == null) {
            throw new ServiceException("Фамилия не может отсутствовать.");
        }
//        if (profileDto.getEmail().isEmpty()) {
////            throw new ServiceException("Адрес почты не может быть пустым.");
//        }
        if (profileDto.getSurname().isEmpty()) {
            throw new ServiceException("Фамилия не может быть пустой.");
        }
        if (profileDto.getName().isEmpty()) {
            throw new ServiceException("Имя не может быть пустым.");
        }
        if (profileDto.getName().length() > 100) {
            throw new ServiceException("Имя не может превышать 100 символов.");
        }
        if (profileDto.getSurname().length() > 100) {
            throw new ServiceException("Фамилия не может превышать 100 символов.");
        }
//        if (profileDto.getEmail().length() > 300) {
////            throw new ServiceException("Адрес почты не может превышать 300 символов.");
//        }

    }

    private SettingsDto defaultSettingDto(Long profileId) {
        final SettingsDto settingsDto = new SettingsDto();
        settingsDto.setProfileId(profileId);
        settingsDto.setWelcomeScreenBackgroundId(1L);
        settingsDto.setProfileScreenBackgroundId(1L);
        settingsDto.setTrainingScreenBackgroundId(1L);
        settingsDto.setNutritionScreenBackgroundId(1L);
        settingsDto.setIdeasScreenBackgroundId(1L);
        settingsDto.setGoalsScreenBackgroundId(1L);
        return settingsDto;
    }

    public ProfileDto changeProfile(ProfileDto profileDto) {
        validate(profileDto);
        final Profile profile = profileDto.profile();
        final Long authorizedUserId = getAuthorizedUserId();
        final Profile profile1 = profileRepository.findByUserId(authorizedUserId);
        profile1.setName(profile.getName());
        profile1.setSurname(profile.getSurname());
        profile1.setAge(profile.getAge());
        profile1.setEmail(profile.getEmail());
        if (!imageRepository.existsById(profile.getImageId())) {
            throw new ServiceException("Не найдено изображение для профиля.");
        } else {
            profile1.setImageId(profile.getImageId());
        }
        final ProfileDto save = profileRepository.save(profile1).dto();
        final SettingsDto settingsDto = profileDto.getSettingsDto();
        validate(settingsDto);
        if (settingsDto != null) {
            settingsDto.setProfileId(save.getId());
            final Settings save1 = settingsRepository.save(settingsDto.settings());
            save.setSettingsDto(save1.dto());
        } else {
            save.setSettingsDto(getProfile().getSettingsDto());
        }
        return save;
    }

    private void validate(SettingsDto settingsDto) {
        if (settingsDto == null) {
            return;
        }
        if (!imageRepository.existsById(settingsDto.getGoalsScreenBackgroundId())) {
            throw new ServiceException("Не найдено изображение заднего фона заданий.");
        }
        if (!imageRepository.existsById(settingsDto.getIdeasScreenBackgroundId())) {
            throw new ServiceException("Не найдено изображение заднего фона идей.");
        }
        if (!imageRepository.existsById(settingsDto.getProfileScreenBackgroundId())) {
            throw new ServiceException("Не найдено изображение заднего фона профиля.");
        }
        if (!imageRepository.existsById(settingsDto.getNutritionScreenBackgroundId())) {
            throw new ServiceException("Не найдено изображение заднего фона еды.");
        }
        if (!imageRepository.existsById(settingsDto.getTrainingScreenBackgroundId())) {
            throw new ServiceException("Не найдено изображение заднего фона тренировок.");
        }
        if (!imageRepository.existsById(settingsDto.getWelcomeScreenBackgroundId())) {
            throw new ServiceException("Не найдено изображение заднего фона приветствия.");
        }
    }

    public void deleteProgressById(Long id) {
        progressRepository.deleteById(id);
    }

    public void deleteAllProgresses() {
        final Profile byUserId = profileRepository.findByUserId(getAuthorizedUserId());
        progressRepository.deleteAllByProfileId(byUserId.getId());
    }

    public ImageDto setGoalsImage(ImageDto imageDto) {
        validate(imageDto);
        final ProfileDto profile = getProfile();
        final SettingsDto settingsDto = profile.getSettingsDto();
        final Image newImage = imageService.save(imageDto).image();
        settingsDto.setGoalsScreenBackgroundId(newImage.getId());
        settingsRepository.save(settingsDto.settings());
        return newImage.dto();
    }

    public ImageDto setIdeasImage(ImageDto imageDto) {
        validate(imageDto);
        final ProfileDto profile = getProfile();
        final SettingsDto settingsDto = profile.getSettingsDto();
        final Image newImage = imageService.save(imageDto).image();
        settingsDto.setIdeasScreenBackgroundId(newImage.getId());
        settingsRepository.save(settingsDto.settings());
        return newImage.dto();
    }

    public ImageDto setNutritionImage(ImageDto imageDto) {
        validate(imageDto);
        final ProfileDto profile = getProfile();
        final SettingsDto settingsDto = profile.getSettingsDto();
        final Image newImage = imageService.save(imageDto).image();
        settingsDto.setNutritionScreenBackgroundId(newImage.getId());
        settingsRepository.save(settingsDto.settings());
        return newImage.dto();
    }

    public ImageDto setTrainingImage(ImageDto imageDto) {
        validate(imageDto);
        final ProfileDto profile = getProfile();
        final SettingsDto settingsDto = profile.getSettingsDto();
        final Image newImage = imageService.save(imageDto).image();
        settingsDto.setTrainingScreenBackgroundId(newImage.getId());
        settingsRepository.save(settingsDto.settings());
        return newImage.dto();
    }

    public ImageDto setProfileBackgroundImage(ImageDto imageDto) {
        validate(imageDto);
        final ProfileDto profile = getProfile();
        final SettingsDto settingsDto = profile.getSettingsDto();
        final Image newImage = imageService.save(imageDto).image();
        settingsDto.setProfileScreenBackgroundId(newImage.getId());
        settingsRepository.save(settingsDto.settings());
        return newImage.dto();
    }

    private void validate(ImageDto imageDto) {
        imageService.validateImage(imageDto);
    }
}
