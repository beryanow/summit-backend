package ru.summit.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import ru.summit.entity.db.ApplicationUser;
import ru.summit.repository.UserRepository;

import java.util.Optional;

public class AuthorizedUserService {

    @Autowired
    private UserRepository userRepository;

    Long getAuthorizedUserId() {
        final UsernamePasswordAuthenticationToken authentication =
                (UsernamePasswordAuthenticationToken) SecurityContextHolder.getContext().getAuthentication();
        final String username = (String) authentication.getPrincipal();
        final Optional<ApplicationUser> byUsername = userRepository.findByUsername(username);
        if (byUsername.isPresent()) {
            return byUsername.get().getId();
        } else {
            return null;
        }
    }
}
