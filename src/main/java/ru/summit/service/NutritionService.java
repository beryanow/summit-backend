package ru.summit.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Service;
import ru.summit.entity.db.Dish;
import ru.summit.entity.db.Ingredient;
import ru.summit.entity.dto.DishDto;
import ru.summit.entity.dto.IngredientDto;
import ru.summit.exception.ServiceException;
import ru.summit.repository.DishRepository;
import ru.summit.repository.ImageRepository;
import ru.summit.repository.IngredientRepository;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import static ru.summit.util.StreamUtil.stream;

@Slf4j
@DependsOn("ImageService")
@Service
public class NutritionService extends AuthorizedUserService {

    private final DishRepository dishRepository;
    private final IngredientRepository ingredientRepository;
    private final ImageRepository imageRepository;

    public NutritionService(DishRepository dishRepository,
                            IngredientRepository ingredientRepository,
                            ImageRepository imageRepository) {
        this.dishRepository = dishRepository;
        this.ingredientRepository = ingredientRepository;
        this.imageRepository = imageRepository;
    }

    public Collection<DishDto> getAll() {
        final List<DishDto> collect = stream(dishRepository.findAllByUserId(getAuthorizedUserId()))
                .map(Dish::dto)
                .collect(Collectors.toList());
        collect.forEach(dishDto -> dishDto.setIngredients(getIngredientsByDishId(dishDto.getId())));
        return collect;
    }

    private Collection<IngredientDto> getIngredientsByDishId(Long id) {
        return stream(ingredientRepository.findAllByDishIdAndUserId(id, getAuthorizedUserId()))
                .map(Ingredient::dto).collect(Collectors.toList());
    }

    public DishDto createOrChange(DishDto dishDto) {
        boolean isNew = dishDto.getId() != null && dishRepository.existsByIdAndUserId(dishDto.getId(), getAuthorizedUserId());
        validateDish(dishDto);
        validateIngredients(dishDto.getIngredients());
        if (!imageRepository.existsById(dishDto.getBackgroundId())) {
            dishDto.setBackgroundId(1L);
        }
        dishDto.setUserId(getAuthorizedUserId());
        final DishDto dto = dishRepository.save(dishDto.dish()).dto();
        if (isNew) {
            ingredientRepository.deleteAllByDishIdAndUserId(dto.getId(), getAuthorizedUserId());
        }
        final Iterable<Ingredient> ingredients = ingredientRepository.saveAll(dishDto.getIngredients()
                .stream()
                .map(ingredientDto -> {
                    ingredientDto.setUserId(getAuthorizedUserId());
                    return ingredientDto.ingredient(dto.getId());
                })
                .collect(Collectors.toList()));
        dto.setIngredients(stream(ingredients)
                .map(Ingredient::dto)
                .collect(Collectors.toList()));
        return dto;
    }

    private void validateDish(DishDto dishDto) {
        if (dishDto.getName().isEmpty()) {
            throw new ServiceException("Название блюда не может быть пустым.");
        }
        if (dishDto.getName().length() > 100) {
            throw new ServiceException("Название блюда не может превышать 100 символов.");
        }
        if (dishDto.getDescription().length() > 500) {
            throw new ServiceException("Описание блюда не может превышать 500 символов.");
        }
        if (dishDto.getRecipe().length() > 10000) {
            throw new ServiceException("Рецепт блюда не может превышать 10000 символов.");
        }
        if (dishDto.getRecipe().isEmpty()) {
            throw new ServiceException("Рецепт блюда не может быть пустым.");
        }
        if (!dishDto.getSize().matches("[12]x[12]")) {
            throw new ServiceException("Размер поля не соответствует требованиям.");
        }
    }

    private void validateIngredients(Collection<IngredientDto> ingredients) {
        for (IngredientDto ing : ingredients) {
            if (ing.getName().isEmpty()) {
                throw new ServiceException("Название ингредиента не может быть пустым.");
            }
            if (ing.getAmount().isEmpty()) {
                throw new ServiceException("Количество ингредиента не может быть пустым.");
            }
            if (ing.getName().length() > 100) {
                throw new ServiceException("Название ингредиента не может превышать 100 символов.");
            }
            if (ing.getAmount().length() > 100) {
                throw new ServiceException("Количество ингредиента не может превышать 100 символов.");
            }
        }
    }

    public void delete(Long dishId) {
        ingredientRepository.deleteAllByDishIdAndUserId(dishId, getAuthorizedUserId());
        dishRepository.deleteByIdAndUserId(dishId, getAuthorizedUserId());
    }
}
