package ru.summit.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.summit.entity.db.Exercise;
import ru.summit.entity.core.Type;
import ru.summit.entity.dto.ExerciseDto;
import ru.summit.entity.dto.ExerciseSpecsDto;
import ru.summit.exception.ServiceException;
import ru.summit.repository.ExerciseRepository;

import java.util.Collection;
import java.util.stream.Collectors;

import static ru.summit.util.StreamUtil.stream;

@Slf4j
@Service
public class ExerciseService {
    private final ExerciseRepository exerciseRepository;

    @Autowired
    public ExerciseService(ExerciseRepository exerciseRepository) {
        this.exerciseRepository = exerciseRepository;
    }

    public ExerciseDto getById(Long id) {
        if (id == null) {
            throw new ServiceException("Id is null");
        }
        return exerciseRepository.findById(id).map(Exercise::dto).orElse(null);
    }

    public Collection<ExerciseDto> findAllByTrainingId(Long trainingId) {
        return stream(exerciseRepository.findAllByTrainingId(trainingId))
                .map(Exercise::dto)
                .collect(Collectors.toList());
    }

    public ExerciseDto create(ExerciseSpecsDto exerciseSpecsDto) {
        final Exercise exercise = new Exercise();
        exercise.setName(exerciseSpecsDto.getName());
        exercise.setDescription(exerciseSpecsDto.getDescription());
        exercise.setType(exerciseSpecsDto.getType());
        exercise.setRepetitions(exerciseSpecsDto.getRepetitions());
        return exercise.dto();
    }

    public void deleteAllByTrainingId(Long id) {
        exerciseRepository.deleteAllByTrainingId(id);
    }

    public Iterable<Exercise> saveAll(Iterable<Exercise> exercises) {
        return exerciseRepository.saveAll(exercises);
    }

    public void validateExercises(Collection<ExerciseDto> exerciseDtos, boolean isOld) {
        if (exerciseDtos == null || exerciseDtos.isEmpty()) {
            throw new ServiceException("В тренировке не могут отсутствовать упражнения.");
        }
        for (ExerciseDto dto : exerciseDtos) {
            if (dto.getName() == null) {
                throw new ServiceException("Название упражнения должно быть задано.");
            }
            if (dto.getName().isEmpty()) {
                throw new ServiceException("Название упражнения не может быть пустым.");
            }
            if (dto.getName().length() > 100) {
                throw new ServiceException("Название упражнения не может превышать 100 символов.");
            }
            if (dto.getDescription() == null) {
                throw new ServiceException("Описание упражнения должно быть задано.");
            }
            if (dto.getDescription().length() > 500) {
                throw new ServiceException("Описание упражнения не может превышать 500 символов.");
            }
            if (dto.getRepetitions() == null) {
                throw new ServiceException("Число повторений упражнения не может быть пустым.");
            }
            if (isOld && dto.getTrainingId() == null) {
                throw new ServiceException("Упражнение не может не принадлежать какой-либо конкретно тренировке.");
            }
            if (dto.getType() == null) {
                throw new ServiceException("Тип упражнения должен быть указан.");
            }
            if (Type.byName(dto.getType()) == null) {
                throw new ServiceException("Неподходящий тип упражнения.");
            }
        }
    }
}
