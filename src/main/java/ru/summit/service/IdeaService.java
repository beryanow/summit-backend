package ru.summit.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Service;
import ru.summit.entity.db.Idea;
import ru.summit.entity.dto.IdeaDto;
import ru.summit.exception.ServiceException;
import ru.summit.repository.IdeaRepository;
import ru.summit.repository.ImageRepository;

import java.util.Collection;
import java.util.stream.Collectors;

import static ru.summit.util.StreamUtil.stream;

@Service
@Slf4j
@DependsOn("ImageService")
public class IdeaService extends AuthorizedUserService {
    private final IdeaRepository ideaRepository;
    private final ImageRepository imageRepository;

    @Autowired
    public IdeaService(IdeaRepository ideaRepository, ImageRepository imageRepository) {
        this.ideaRepository = ideaRepository;
        this.imageRepository = imageRepository;
    }

    public Collection<IdeaDto> getAll() {
        return stream(ideaRepository.findAllByUserId(getAuthorizedUserId()))
                .map(Idea::dto)
                .collect(Collectors.toList());
    }

    public IdeaDto create(IdeaDto ideaDto) {
        validateIdea(ideaDto);
        if (!imageRepository.existsById(ideaDto.getBackgroundId())) {
            ideaDto.setBackgroundId(1L);
        }
        if (ideaDto.getSize() == null || ideaDto.getSize().isEmpty()) {
            ideaDto.setSize("1x1");
        }
        ideaDto.setUserId(getAuthorizedUserId());
        return ideaRepository.save(ideaDto.idea()).dto();
    }

    private void validateIdea(IdeaDto ideaDto) {
        if (ideaDto.getName() == null) {
            throw new ServiceException("Название идеи не может отсутствовать.");
        }
        if (ideaDto.getContent() == null) {
            throw new ServiceException("Текст описания идеи не может отсутствовать.");
        }
        if (ideaDto.getContent().length() > 1000) {
            throw new ServiceException("Текст описания идеи не может превышать 1000 символов.");
        }
        if (ideaDto.getName().isEmpty()) {
            throw new ServiceException("Название идеи не может быть пустым.");
        }
        if (ideaDto.getName().length() > 100) {
            throw new ServiceException("Название идеи не может превышать 100 символов.");
        }
        if (!ideaDto.getSize().matches("[12]x[12]")) {
            throw new ServiceException("Размер поля не соответствует требованиям.");
        }
    }

    public void deleteById(Long id) {
        ideaRepository.deleteByIdAndUserId(id, getAuthorizedUserId());
    }
}
