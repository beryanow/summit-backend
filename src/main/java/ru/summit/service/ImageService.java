package ru.summit.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.DependsOn;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Service;
import ru.summit.entity.db.Image;
import ru.summit.entity.dto.ImageDto;
import ru.summit.exception.ServiceException;
import ru.summit.repository.ImageRepository;

import javax.annotation.PostConstruct;
import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;

@Slf4j
@Service("ImageService")
@DependsOn("databaseConfig")
public class ImageService {
    private final ImageRepository imageRepository;
    private final ResourceLoader resourceLoader;

    @Autowired
    public ImageService(ImageRepository imageRepository, ResourceLoader resourceLoader) {
        this.imageRepository = imageRepository;
        this.resourceLoader = resourceLoader;
    }

    @PostConstruct
    public void init() {
        loadImage("classpath:/images/app.jpeg", 1L);
        loadImage("classpath:/images/app1.jpg", 2L);
        loadImage("classpath:/images/face.jpg", 3L);
        loadImage("classpath:/images/fried_egg.jpg", 4L);
    }

    private void loadImage(String location, Long id) {
        try {
            final Resource resource = resourceLoader.getResource(location);
            final ByteBuffer allocate = ByteBuffer.allocate((int) resource.contentLength());
            while (resource.readableChannel().read(allocate) > 0) {
            }
            final byte[] content = allocate.array();
            final Image image = new Image();
            image.setId(id);
            image.setContent(content);
            log.info("ID сохраненной фотки {}: {}", resource.getFilename(), imageRepository.save(image).getId());
        } catch (IOException e) {
            log.warn("Не вышло инициализировать изображение.", e);
        }
    }

    public ImageDto getById(Long id) {
        if (id == null) {
            throw new ServiceException("Id is null");
        }
        return imageRepository.findById(id).map(Image::dto).orElse(null);
    }

    public ImageDto save(ImageDto imageDto) {
        imageDto.setId(null);
        validateImage(imageDto);
        byte[] content = imageDto.getContent();
        try {
            content = toJpgBytes(imageDto.getContent());
        } catch (IOException e) {
            log.warn("Неожиданная ошибка конвертации изображения. Изображение будет сохранено в оригинальном виде", e);
        }
        imageDto.setContent(content);
        return imageRepository.save(imageDto.image()).dto();
    }

    public byte[] toJpgBytes(byte[] bytes) throws IOException {
        ByteArrayInputStream bais = new ByteArrayInputStream(bytes);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        BufferedImage input = ImageIO.read(bais);
        int width = input.getWidth();
        int height = input.getHeight();
        BufferedImage output = new BufferedImage(width, height, BufferedImage.TYPE_3BYTE_BGR);
        int[] px = new int[width * height];
        input.getRGB(0, 0, width, height, px, 0, width);
        output.setRGB(0, 0, width, height, px, 0, width);
        ImageIO.write(output, "jpg", baos);
        return baos.toByteArray();
    }

    public void validateImage(ImageDto imageDto) {
        if (imageDto.getContent() == null || imageDto.getContent().length == 0 ) {
            throw new ServiceException("Изображение не может быть пустым или отсутствовать.");
        }
        if (imageDto.getContent().length > 1024 * 1024 * 2) {
            throw new ServiceException("Размер исходного изображения не должен превышать 2МБ.");
        }
    }
}
