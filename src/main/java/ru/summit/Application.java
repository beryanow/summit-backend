package ru.summit;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import ru.summit.controller.AuthController;
import ru.summit.entity.db.Dish;
import ru.summit.entity.db.Goal;
import ru.summit.entity.db.Task;
import ru.summit.entity.dto.DishDto;
import ru.summit.entity.dto.IdeaDto;
import ru.summit.entity.dto.IngredientDto;
import ru.summit.entity.dto.UserDto;
import ru.summit.exception.ServiceException;
import ru.summit.repository.*;

import static ru.summit.entity.core.Stage.IN_PROGRESS;
import static ru.summit.entity.core.Stage.NEW;

@Slf4j
@SpringBootApplication
public class Application {
    public static void main(String[] args) {
        final ConfigurableApplicationContext context = SpringApplication.run(Application.class, args);
        initWithTestData(context);
    }

    public static void initWithTestData(final ConfigurableApplicationContext context) {
        final AuthController authController = (AuthController) context.getBean("authController");
        final DishRepository dishRepository = (DishRepository) context.getBean("dishRepository");
        final IngredientRepository ingredientRepository = (IngredientRepository) context.getBean("ingredientRepository");
        final IdeaRepository ideaRepository = (IdeaRepository) context.getBean("ideaRepository");
        final GoalRepository goalRepository = (GoalRepository) context.getBean("goalRepository");
        final TaskRepository taskRepository = (TaskRepository) context.getBean("taskRepository");
        final UserDto userDto = new UserDto();
        userDto.setAge(20);
        userDto.setName("Test");
        userDto.setSurname("Testovin");
        userDto.setEmail("email@test.net");
        userDto.setUsername("username");
        userDto.setPassword("qwerty");
        try {
            final Long userId = authController.signUp(userDto);
            /*Добавляем рецепт*/
            DishDto dishDto = new DishDto();
            dishDto.setName("Яичница");
            dishDto.setRecipe("Налейте растительное масло в чистую сковороду и поставьте на огонь.\n" +
                    "Разбейте яйца и вылейте их на сковороду, стараясь не повредить желток.\n" +
                    "Посолите по вкусу и жарьте 2-3 минуты. Лопаткой аккуратно переложите яичницу на тарелку и подавайте на стол.");
            dishDto.setDescription("Самый быстрый и простой завтрак. Жареные яйца — прекрасное блюдо для тех, кто не любит долго стоять у плиты.");
            dishDto.setBackgroundId(4L);
            dishDto.setSize("2x2");
            dishDto.setUserId(userId);
            final Dish save = dishRepository.save(dishDto.dish());
            log.info("Была сохранена сущность {}", save);
            final IngredientDto ingredientDto1 = new IngredientDto();
            ingredientDto1.setName("Яйца");
            ingredientDto1.setAmount("3 штуки");
            ingredientDto1.setUserId(userId);
            final IngredientDto ingredientDto2 = new IngredientDto();
            ingredientDto2.setName("Растительное масло");
            ingredientDto2.setAmount("1 ст. ложка");
            ingredientDto2.setUserId(userId);
            final IngredientDto ingredientDto3 = new IngredientDto();
            ingredientDto3.setName("Соль");
            ingredientDto3.setAmount("По вкусу");
            ingredientDto3.setUserId(userId);
            log.info("Была сохранена сущность {}", ingredientRepository.save(ingredientDto1.ingredient(save.getId())));
            log.info("Была сохранена сущность {}", ingredientRepository.save(ingredientDto2.ingredient(save.getId())));
            log.info("Была сохранена сущность {}", ingredientRepository.save(ingredientDto3.ingredient(save.getId())));
            /*Добавляем идею*/
            final IdeaDto dto = new IdeaDto();
            dto.setId(1L);
            dto.setBackgroundId(2L);
            dto.setName("Классная идея");
            dto.setContent("Можно вместо свечек использовать зажигалки!");
            dto.setSize("1x1");
            dto.setUserId(userId);
            log.info("Была сохранена сущность {}", ideaRepository.save(dto.idea()));
            /*Добавляем цель*/
            final Goal goal = new Goal();
            goal.setName("Запрогать сервер");
            goal.setDescription("Для саммита надо сделать сервер.\nМеня ждут великие дела! :D");
            goal.setStatePlace("Not implemented");
            goal.setBackgroundId(2L);
            goal.setSize("2x2");
            goal.setUserId(userId);
            Task task1 = new Task();
            task1.setName("Сделать API");
            task1.setDescription("Надо сделать все контроллеры и логику, не забывая логировать время в Redmine.");
            task1.setStage(IN_PROGRESS.toString());
            task1.setUserId(userId);
            Task task2 = new Task();
            task2.setName("Починить API");
            task2.setDescription("Там много проблем, но я справлюсь!");
            task2.setUserId(userId);
            task2.setStage(NEW.toString());
            final Goal save1 = goalRepository.save(goal);
            log.info("Была сохранена сущность {}", save1);
            task1.setGoalId(save1.getId());
            task2.setGoalId(save1.getId());
            log.info("Была сохранена сущность {}", taskRepository.save(task1));
            log.info("Была сохранена сущность {}", taskRepository.save(task2));
        } catch (ServiceException e) {
            log.info(e.getMessage());
        }
    }

    @Bean
    public BCryptPasswordEncoder bCryptPasswordEncoder() {
        return new BCryptPasswordEncoder();
    }
}
